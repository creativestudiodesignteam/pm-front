import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
/* import Menu from './components/Menu'; */
import Footer from './components/Footer';

/* import Home from './pages/Home'; */
import Login from './pages/Login';
import Register from './pages/Register';
import UserEdit from './pages/UserEdit';


export default function App() {

  return (
    <BrowserRouter>
      <Switch>
        <Route path='/login' exact component={Login} />
        <Route path='/register' exact component={Register} />
        <Route path="/">
          <Header />
          {/* <Menu /> */}
          <Route path="/" exact component={UserEdit} />
          {/* <Route path='/user/edit' component={UserEdit} /> */}
          <div className="sidenav-overlay"></div>
          <div className="drag-target"></div>
          <Footer />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}
