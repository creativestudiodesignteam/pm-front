import React, { useState, useEffect } from 'react';
import swal from 'sweetalert';
import { Link, useHistory } from 'react-router-dom';
import StringMask from 'string-mask';

import serverApi from '../../services/api';
import viacep from '../../services/viacep';
/* import functions from '../../functions'; */

import './styles.css';

function Register() {
    const history = useHistory();

/*     useEffect(() => {
        functions.checkLogin().then(login => {
            if (login) return history.push('/');
        }); 
    });
 */
    const [cadastro, setCadastro] = useState({
        razaoSocial: '',
        nomeFantasia: '',
        cnpj: '',
        inscricaoEstadual: '',
        cep: '',
        logradouro: '',
        numero: '',
        complemento: '',
        bairro: '',
        cidade: '',
        uf: '',
        email: '',
        telefone: '',
        precoPeso: '',
        precoCubagem: '',
        precoDistancia: '',
        entregaPessoaFisica: false,
        senha: '',
    });

    const [confirmeSenha, setConfirmeSenha] = useState('');

    useEffect(() => {
        //Mask CPNJ
        let cnpj = cadastro.cnpj;
        cnpj = cnpj.replace(/[^\d]+/g, '');
        cnpj = (new StringMask('00.000.000/0000-00')).apply(cnpj);
        if (cnpj !== cadastro.cnpj) {
            setCadastro({ ...cadastro, cnpj });
        }

        //Mask Inscrição Estadual
        let inscricaoEstadual = cadastro.inscricaoEstadual;
        inscricaoEstadual = inscricaoEstadual.replace(/[^\d\w]+/g, '')
            .toUpperCase();
        if (inscricaoEstadual !== cadastro.inscricaoEstadual) {
            setCadastro({ ...cadastro, inscricaoEstadual });
        }

        //Mask CPNJ
        let cep = cadastro.cep;
        cep = cep.replace(/[^\d]+/g, '');
        cep = (new StringMask('00000-000')).apply(cep);
        if (cep !== cadastro.cep) {
            setCadastro({ ...cadastro, cep });
        }

        //Mask Número
        let numero = cadastro.numero;
        numero = numero.replace(/[^\d]+/g, '');
        if (numero !== cadastro.numero) {
            setCadastro({ ...cadastro, numero });
        }

        //Mask Telefone
        let telefone = cadastro.telefone;
        telefone = telefone.replace(/[^\d]+/g, '');
        telefone = (new StringMask('(00) 90000-0000')).apply(telefone);
        if (telefone !== cadastro.telefone) {
            setCadastro({ ...cadastro, telefone });
        }
    }, [cadastro]);

    function consultarCEP(noswal = false) {
        if (!cadastro.cep) {
            if (noswal) return;
            return swal('Ops!', 'CEP inválido!', 'error');
        }

        const cep = cadastro.cep.replace(/[^\d]+/g, '');

        if (cep.length !== 8) {
            if (noswal) return;
            return swal('Ops!', 'CEP inválido!', 'error');
        }

        viacep.buscarCEP(cep)
            .then(response => {
                if (response.success) {
                    setCadastro({
                        ...cadastro,
                        logradouro: response.endereco.logradouro,
                        numero: '',
                        complemento: '',
                        bairro: response.endereco.bairro,
                        cidade: response.endereco.localidade,
                        uf: response.endereco.uf
                    });
                }

                if (noswal) return;
                return swal('Ops!', response.error, 'error');
            });
    }

    async function handleRegister(e) {
        e.preventDefault();

        if (cadastro.senha !== confirmeSenha)
            return swal('Ops!', 'As senha não são iguais.', 'error');

        const response = await serverApi.register({ ...cadastro });

        if (response.success) {
            swal('Pronto!', 'Cadastro realizado com sucesso!', 'success');
            history.push('/login');
        } else {
            swal('Ops!', response.error, 'error');
        }
    }

    return (
        <div className="app-content content register">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
            <div className="content-wrapper">
                <div className="content-body">
                    <section className="row flexbox-container justify-content-center">
                        <div className="col-xl-8 col-10 d-flex justify-content-center">
                            <div className="card bg-authentication rounded-0 mb-0 w-100">
                                <div className="row m-0">
                                    <div className="col-12 p-0">
                                        <div className="card rounded-0 mb-0 p-2">
                                            <div className="card-header pt-50 pb-1">
                                                <div className="card-title">
                                                    <h4 className="mb-0">Cadastro</h4>
                                                </div>
                                            </div>
                                            <p className="px-2">Preencha todos os campos abaixo.</p>
                                            <div className="card-content">
                                                <div className="card-body pt-0">
                                                    <form onSubmit={handleRegister}>
                                                        <div className="row mt-1" style={{ margin: "0 -1rem" }}>
                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input1"
                                                                        className="form-control"
                                                                        placeholder="Razão Social"
                                                                        value={cadastro.razaoSocial}
                                                                        onChange={e => setCadastro({ ...cadastro, razaoSocial: e.target.value })}
                                                                        required />
                                                                    <label for="input1">Razão Social</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input2"
                                                                        className="form-control"
                                                                        placeholder="Nome Fantasia"
                                                                        value={cadastro.nomeFantasia}
                                                                        onChange={e => setCadastro({ ...cadastro, nomeFantasia: e.target.value })}
                                                                        required />
                                                                    <label for="input2">Nome Fantasia</label>
                                                                </div>
                                                            </div>

                                                            <div className="w-100 mb-1"></div>

                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input3"
                                                                        className="form-control"
                                                                        placeholder="CNPJ"
                                                                        value={cadastro.cnpj}
                                                                        onChange={e => setCadastro({ ...cadastro, cnpj: e.target.value })}
                                                                        required />
                                                                    <label for="input3">CNPJ</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input4"
                                                                        className="form-control"
                                                                        placeholder="Inscrição Estadual"
                                                                        value={cadastro.inscricaoEstadual}
                                                                        onChange={e => setCadastro({ ...cadastro, inscricaoEstadual: e.target.value })}
                                                                        required />
                                                                    <label for="input4">Inscrição Estadual</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h5 className="mb-3 mt-2">Endereço</h5>

                                                        <div className="row mt-1" style={{ margin: "0 -1rem" }}>
                                                            <div className="col-lg-4 col px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input5"
                                                                        className="form-control"
                                                                        placeholder="CEP"
                                                                        value={cadastro.cep}
                                                                        onChange={e => setCadastro({ ...cadastro, cep: e.target.value })}
                                                                        onBlur={e => consultarCEP(true)}
                                                                        required />
                                                                    <label for="input5">CEP</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-auto px-1">
                                                                <button type="button" className="btn btn-primary" onClick={e => consultarCEP()}>Buscar CEP</button>
                                                            </div>

                                                            <div className="w-100 mb-1"></div>

                                                            <div className="col-lg-5 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input6"
                                                                        className="form-control"
                                                                        placeholder="Logradouro"
                                                                        value={cadastro.logradouro}
                                                                        onChange={e => setCadastro({ ...cadastro, logradouro: e.target.value })}
                                                                        required />
                                                                    <label for="input6">Logradouro</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input7"
                                                                        className="form-control"
                                                                        placeholder="Número"
                                                                        value={cadastro.numero}
                                                                        onChange={e => setCadastro({ ...cadastro, numero: e.target.value })} />
                                                                    <label for="input7">Número</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-5 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input8"
                                                                        className="form-control"
                                                                        placeholder="Complemento"
                                                                        value={cadastro.complemento}
                                                                        onChange={e => setCadastro({ ...cadastro, complemento: e.target.value })} />
                                                                    <label for="input8">Complemento</label>
                                                                </div>
                                                            </div>

                                                            <div className="w-100 mb-1"></div>

                                                            <div className="col-lg-5 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input9"
                                                                        className="form-control"
                                                                        placeholder="Bairro"
                                                                        value={cadastro.bairro}
                                                                        onChange={e => setCadastro({ ...cadastro, bairro: e.target.value })}
                                                                        required />
                                                                    <label for="input9">Bairro</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-5 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input10"
                                                                        className="form-control"
                                                                        placeholder="Cidade"
                                                                        value={cadastro.cidade}
                                                                        onChange={e => setCadastro({ ...cadastro, cidade: e.target.value })}
                                                                        required />
                                                                    <label for="input10">Cidade</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-12 px-1">

                                                                <div className="form-label-group">
                                                                    <select className="form-control"
                                                                        id="input11"
                                                                        value={cadastro.uf}
                                                                        onChange={e => setCadastro({ ...cadastro, uf: e.target.value })}
                                                                        required>
                                                                        <option value="">Selecione</option>
                                                                        <option value="AC">AC</option>
                                                                        <option value="AL">AL</option>
                                                                        <option value="AP">AP</option>
                                                                        <option value="AM">AM</option>
                                                                        <option value="BA">BA</option>
                                                                        <option value="CE">CE</option>
                                                                        <option value="DF">DF</option>
                                                                        <option value="ES">ES</option>
                                                                        <option value="GO">GO</option>
                                                                        <option value="MA">MA</option>
                                                                        <option value="MT">MT</option>
                                                                        <option value="MS">MS</option>
                                                                        <option value="MG">MG</option>
                                                                        <option value="PA">PA</option>
                                                                        <option value="PB">PB</option>
                                                                        <option value="PR">PR</option>
                                                                        <option value="PE">PE</option>
                                                                        <option value="PI">PI</option>
                                                                        <option value="RJ">RJ</option>
                                                                        <option value="RN">RN</option>
                                                                        <option value="RS">RS</option>
                                                                        <option value="RO">RO</option>
                                                                        <option value="RR">RR</option>
                                                                        <option value="SC">SC</option>
                                                                        <option value="SP">SP</option>
                                                                        <option value="SE">SE</option>
                                                                        <option value="TO">TO</option>
                                                                    </select>
                                                                    <label for="input11" style={{
                                                                        padding: '0.25rem 0',
                                                                        fontSize: '0.7rem',
                                                                        top: -20,
                                                                        left: 3,
                                                                        opacity: 1
                                                                    }}>UF</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h5 className="mb-3 mt-2">Contato</h5>

                                                        <div className="row mt-1" style={{ margin: "0 -1rem" }}>
                                                            <div className="col-8 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="email"
                                                                        id="input12"
                                                                        className="form-control"
                                                                        placeholder="E-mail"
                                                                        value={cadastro.email}
                                                                        onChange={e => setCadastro({ ...cadastro, email: e.target.value })}
                                                                        required />
                                                                    <label for="input12">E-mail</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="text"
                                                                        id="input13"
                                                                        className="form-control"
                                                                        placeholder="Telefone"
                                                                        value={cadastro.telefone}
                                                                        onChange={e => setCadastro({ ...cadastro, telefone: e.target.value })}
                                                                        required />
                                                                    <label for="input13">Telefone</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h5 className="mb-3 mt-2">Valor do frete</h5>

                                                        <div className="row mt-1" style={{ margin: "0 -1rem" }}>
                                                            <div className="col-lg-4 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="number"
                                                                        step={0.01}
                                                                        min={0}
                                                                        id="input14"
                                                                        className="form-control"
                                                                        placeholder="Preço do Peso (R$/Kg)"
                                                                        value={cadastro.precoPeso}
                                                                        onChange={e => setCadastro({ ...cadastro, precoPeso: e.target.value })}
                                                                        required />
                                                                    <label for="input14">Preço do Peso (R$/kg)</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-4 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="number"
                                                                        step={0.01}
                                                                        min={0}
                                                                        id="input15"
                                                                        className="form-control"
                                                                        placeholder="Preço da Cubagem (R$/m3)"
                                                                        value={cadastro.precoCubagem}
                                                                        onChange={e => setCadastro({ ...cadastro, precoCubagem: e.target.value })}
                                                                        required />
                                                                    <label for="input15">Preço da Cubagem (R$/m3)</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-4 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="number"
                                                                        step={0.01}
                                                                        min={0}
                                                                        id="input16"
                                                                        className="form-control"
                                                                        placeholder="Preço da Distância (R$/km)"
                                                                        value={cadastro.precoDistancia}
                                                                        onChange={e => setCadastro({ ...cadastro, precoDistancia: e.target.value })}
                                                                        required />
                                                                    <label for="input16">Preço do Distância (R$/km)</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-8 col-12 px-1">
                                                                <fieldset className="checkbox" style={{ padding: '5.5px 0' }}>
                                                                    <div className="vs-checkbox-con vs-checkbox-primary">
                                                                        <input type="checkbox"
                                                                            value={cadastro.entregaPessoaFisica}
                                                                            onChange={e => setCadastro({ ...cadastro, entregaPessoaFisica: e.target.checked })} />
                                                                        <span className="vs-checkbox">
                                                                            <span className="vs-checkbox--check">
                                                                                <i className="vs-icon feather icon-check"></i>
                                                                            </span>
                                                                        </span>
                                                                        <span className=""> Entrega para pessoa física.</span>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>

                                                        <h5 className="mb-3 mt-2">Autenticação</h5>

                                                        <div className="row mt-1" style={{ margin: "0 -1rem" }}>
                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="password"
                                                                        id="input17"
                                                                        className="form-control"
                                                                        placeholder="Senha"
                                                                        value={cadastro.senha}
                                                                        onChange={e => setCadastro({ ...cadastro, senha: e.target.value })}
                                                                        required />
                                                                    <label for="input17">Senha</label>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-12 px-1">
                                                                <div className="form-label-group">
                                                                    <input type="password"
                                                                        id="input18"
                                                                        className="form-control"
                                                                        placeholder="Confirme a senha"
                                                                        value={confirmeSenha}
                                                                        onChange={e => setConfirmeSenha(e.target.value)}
                                                                        required />
                                                                    <label for="input18">Confirme a senha</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="mb-2"></div>

                                                        <Link to="/login" className="btn btn-outline-primary float-left btn-inline mb-50">Entrar</Link>
                                                        <button type="submit" className="btn btn-primary float-right btn-inline mb-50">Cadastrar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div >
    );
}

export default Register;