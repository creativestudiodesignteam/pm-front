import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import functions from '../../functions';
import { useHistory } from 'react-router-dom';

// import { Container } from './styles';

function Home() {
    const empresa = useSelector(state => state.empresa);
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        functions.checkLogin().then(login => {
            if (!login) return history.push('/');
            if (JSON.stringify(login) !== JSON.stringify(empresa)) {
                dispatch({ type: 'SET_EMPRESA', empresa: login });
            }
        });
    });

    return (
        <div className="app-content content">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
            <div className="content-wrapper">
                <div className="content-header row">
                </div>
                <div className="content-body">
                    <section id="dashboard-analytics">
                        <div className="row">
                            <div className="col-lg-6 col-md-12 col-sm-12">
                                <div className="card bg-analytics text-white">
                                    <div className="card-content">
                                        <div className="card-body text-center">
                                            <img src="./app-assets/images/elements/decore-left.png" className="img-left" alt="card-img-left" />
                                            <img src="./app-assets/images/elements/decore-right.png" className="img-right" alt="card-img-right" />
                                            <div className="avatar avatar-xl bg-primary shadow mt-0">
                                                <div className="avatar-content">
                                                    <i className="feather icon-award white font-large-1"></i>
                                                </div>
                                            </div>
                                            <div className="text-center">
                                                <h1 className="mb-2 text-white">Congratulations John,</h1>
                                                <p className="m-auto w-75">You have done <strong>57.6%</strong> more sales today. Check your new badge in your profile.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-12">
                                <div className="card">
                                    <div className="card-header d-flex flex-column align-items-start pb-0">
                                        <div className="avatar bg-rgba-primary p-50 m-0">
                                            <div className="avatar-content">
                                                <i className="feather icon-users text-primary font-medium-5"></i>
                                            </div>
                                        </div>
                                        <h2 className="text-bold-700 mt-1 mb-25">92.6k</h2>
                                        <p className="mb-0">Subscribers Gained</p>
                                    </div>
                                    <div className="card-content">
                                        <div id="subscribe-gain-chart"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-12">
                                <div className="card">
                                    <div className="card-header d-flex flex-column align-items-start pb-0">
                                        <div className="avatar bg-rgba-warning p-50 m-0">
                                            <div className="avatar-content">
                                                <i className="feather icon-package text-warning font-medium-5"></i>
                                            </div>
                                        </div>
                                        <h2 className="text-bold-700 mt-1 mb-25">97.5K</h2>
                                        <p className="mb-0">Orders Received</p>
                                    </div>
                                    <div className="card-content">
                                        <div id="orders-received-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <div className="card-content">
                                        <div className="card-body">
                                            <div className="row pb-50">
                                                <div className="col-lg-6 col-12 d-flex justify-content-between flex-column order-lg-1 order-2 mt-lg-0 mt-2">
                                                    <div>
                                                        <h2 className="text-bold-700 mb-25">2.7K</h2>
                                                        <p className="text-bold-500 mb-75">Avg Sessions</p>
                                                        <h5 className="font-medium-2">
                                                            <span className="text-success">+5.2% </span>
                                                            <span>vs last 7 days</span>
                                                        </h5>
                                                    </div>
                                                    <a href="# " className="btn btn-primary shadow">View Details <i className="feather icon-chevrons-right"></i></a>
                                                </div>
                                                <div className="col-lg-6 col-12 d-flex justify-content-between flex-column text-right order-lg-2 order-1">
                                                    <div className="dropdown chart-dropdown">
                                                        <button className="btn btn-sm border-0 dropdown-toggle p-0" type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Last 7 Days
                                                </button>
                                                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                                                            <a className="dropdown-item" href="# ">Last 28 Days</a>
                                                            <a className="dropdown-item" href="# ">Last Month</a>
                                                            <a className="dropdown-item" href="# ">Last Year</a>
                                                        </div>
                                                    </div>
                                                    <div id="avg-session-chart"></div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row avg-sessions pt-50">
                                                <div className="col-6">
                                                    <p className="mb-0">Goal: $100000</p>
                                                    <div className="progress progress-bar-primary mt-25">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style={{ width: '50%' }}></div>
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <p className="mb-0">Users: 100K</p>
                                                    <div className="progress progress-bar-warning mt-25">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="60" aria-valuemax="100" style={{ width: '60%' }}></div>
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <p className="mb-0">Retention: 90%</p>
                                                    <div className="progress progress-bar-danger mt-25">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="70" aria-valuemax="100" style={{ width: '70%' }}></div>
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <p className="mb-0">Duration: 1yr</p>
                                                    <div className="progress progress-bar-success mt-25">
                                                        <div className="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="90" aria-valuemax="100" style={{ width: '90%' }}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <div className="card-header d-flex justify-content-between pb-0">
                                        <h4 className="card-title">Support Tracker</h4>
                                        <div className="dropdown chart-dropdown">
                                            <button className="btn btn-sm border-0 dropdown-toggle p-0" type="button" id="dropdownItem4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Last 7 Days
                                    </button>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem4">
                                                <a className="dropdown-item" href="# ">Last 28 Days</a>
                                                <a className="dropdown-item" href="# ">Last Month</a>
                                                <a className="dropdown-item" href="# ">Last Year</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-content">
                                        <div className="card-body pt-0">
                                            <div className="row">
                                                <div className="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
                                                    <h1 className="font-large-2 text-bold-700 mt-2 mb-0">163</h1>
                                                    <small>Tickets</small>
                                                </div>
                                                <div className="col-sm-10 col-12 d-flex justify-content-center">
                                                    <div id="support-tracker-chart"></div>
                                                </div>
                                            </div>
                                            <div className="chart-info d-flex justify-content-between">
                                                <div className="text-center">
                                                    <p className="mb-50">New Tickets</p>
                                                    <span className="font-large-1">29</span>
                                                </div>
                                                <div className="text-center">
                                                    <p className="mb-50">Open Tickets</p>
                                                    <span className="font-large-1">63</span>
                                                </div>
                                                <div className="text-center">
                                                    <p className="mb-50">Response Time</p>
                                                    <span className="font-large-1">1d</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row match-height">
                            <div className="col-lg-4 col-12">
                                <div className="card">
                                    <div className="card-header d-flex justify-content-between pb-0">
                                        <h4>Product Orders</h4>
                                        <div className="dropdown chart-dropdown">
                                            <button className="btn btn-sm border-0 dropdown-toggle p-0" type="button" id="dropdownItem2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Last 7 Days
                                    </button>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem2">
                                                <a className="dropdown-item" href="# ">Last 28 Days</a>
                                                <a className="dropdown-item" href="# ">Last Month</a>
                                                <a className="dropdown-item" href="# ">Last Year</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-content">
                                        <div className="card-body">
                                            <div id="product-order-chart" className="mb-3"></div>
                                            <div className="chart-info d-flex justify-content-between mb-1">
                                                <div className="series-info d-flex align-items-center">
                                                    <i className="fa fa-circle-o text-bold-700 text-primary"></i>
                                                    <span className="text-bold-600 ml-50">Finished</span>
                                                </div>
                                                <div className="product-result">
                                                    <span>23043</span>
                                                </div>
                                            </div>
                                            <div className="chart-info d-flex justify-content-between mb-1">
                                                <div className="series-info d-flex align-items-center">
                                                    <i className="fa fa-circle-o text-bold-700 text-warning"></i>
                                                    <span className="text-bold-600 ml-50">Pending</span>
                                                </div>
                                                <div className="product-result">
                                                    <span>14658</span>
                                                </div>
                                            </div>
                                            <div className="chart-info d-flex justify-content-between mb-75">
                                                <div className="series-info d-flex align-items-center">
                                                    <i className="fa fa-circle-o text-bold-700 text-danger"></i>
                                                    <span className="text-bold-600 ml-50">Rejected</span>
                                                </div>
                                                <div className="product-result">
                                                    <span>4758</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-12">
                                <div className="card">
                                    <div className="card-header d-flex justify-content-between align-items-start">
                                        <div>
                                            <h4 className="card-title">Sales Stats</h4>
                                            <p className="text-muted mt-25 mb-0">Last 6 months</p>
                                        </div>
                                        <p className="mb-0"><i className="feather icon-more-vertical font-medium-3 text-muted cursor-pointer"></i></p>
                                    </div>
                                    <div className="card-content">
                                        <div className="card-body px-0">
                                            <div id="sales-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title">Activity Timeline</h4>
                                    </div>
                                    <div className="card-content">
                                        <div className="card-body">
                                            <ul className="activity-timeline timeline-left list-unstyled">
                                                <li>
                                                    <div className="timeline-icon bg-primary">
                                                        <i className="feather icon-plus font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div className="timeline-info">
                                                        <p className="font-weight-bold mb-0">Client Meeting</p>
                                                        <span className="font-small-3">Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                                    </div>
                                                    <small className="text-muted">25 mins ago</small>
                                                </li>
                                                <li>
                                                    <div className="timeline-icon bg-warning">
                                                        <i className="feather icon-alert-circle font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div className="timeline-info">
                                                        <p className="font-weight-bold mb-0">Email Newsletter</p>
                                                        <span className="font-small-3">Cupcake gummi bears soufflé caramels candy</span>
                                                    </div>
                                                    <small className="text-muted">15 days ago</small>
                                                </li>
                                                <li>
                                                    <div className="timeline-icon bg-danger">
                                                        <i className="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div className="timeline-info">
                                                        <p className="font-weight-bold mb-0">Plan Webinar</p>
                                                        <span className="font-small-3">Candy ice cream cake. Halvah gummi bears</span>
                                                    </div>
                                                    <small className="text-muted">20 days ago</small>
                                                </li>
                                                <li>
                                                    <div className="timeline-icon bg-success">
                                                        <i className="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div className="timeline-info">
                                                        <p className="font-weight-bold mb-0">Launch Website</p>
                                                        <span className="font-small-3">Candy ice cream cake. </span>
                                                    </div>
                                                    <small className="text-muted">25 days ago</small>
                                                </li>
                                                <li>
                                                    <div className="timeline-icon bg-primary">
                                                        <i className="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div className="timeline-info">
                                                        <p className="font-weight-bold mb-0">Marketing</p>
                                                        <span className="font-small-3">Candy ice cream. Halvah bears Cupcake gummi bears.</span>
                                                    </div>
                                                    <small className="text-muted">28 days ago</small>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="mb-0">Dispatched Orders</h4>
                                    </div>
                                    <div className="card-content">
                                        <div className="table-responsive mt-1">
                                            <table className="table table-hover-animation mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>ORDER</th>
                                                        <th>STATUS</th>
                                                        <th>OPERATORS</th>
                                                        <th>LOCATION</th>
                                                        <th>DISTANCE</th>
                                                        <th>START DATE</th>
                                                        <th>EST DEL. DT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>#879985</td>
                                                        <td><i className="fa fa-circle font-small-3 text-success mr-50"></i>Moving</td>
                                                        <td className="p-1">
                                                            <ul className="list-unstyled users-list m-0  d-flex align-items-center">
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Vinnie Mostowy" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-5.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Elicia Rieske" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Julee Rossignol" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-10.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Darcey Nooner" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-8.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>Anniston, Alabama</td>
                                                        <td>
                                                            <span>130 km</span>
                                                            <div className="progress progress-bar-success mt-1 mb-0">
                                                                <div className="progress-bar" role="progressbar" style={{ width: '80%' }} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </td>
                                                        <td>14:58 26/07/2018</td>
                                                        <td>28/07/2018</td>
                                                    </tr>
                                                    <tr>
                                                        <td>#156897</td>
                                                        <td><i className="fa fa-circle font-small-3 text-warning mr-50"></i>Pending</td>
                                                        <td className="p-1">
                                                            <ul className="list-unstyled users-list m-0  d-flex align-items-center">
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Trina Lynes" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-1.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Lilian Nenez" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-2.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Alberto Glotzbach" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-3.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>Cordova, Alaska</td>
                                                        <td>
                                                            <span>234 km</span>
                                                            <div className="progress progress-bar-warning mt-1 mb-0">
                                                                <div className="progress-bar" role="progressbar" style={{ width: '60%' }} aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </td>
                                                        <td>14:58 26/07/2018</td>
                                                        <td>28/07/2018</td>
                                                    </tr>
                                                    <tr>
                                                        <td>#568975</td>
                                                        <td><i className="fa fa-circle font-small-3 text-success mr-50"></i>Moving</td>
                                                        <td className="p-1">
                                                            <ul className="list-unstyled users-list m-0  d-flex align-items-center">
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Lai Lewandowski" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-6.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Elicia Rieske" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Darcey Nooner" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-8.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Julee Rossignol" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-10.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Jeffrey Gerondale" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-9.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>Florence, Alabama</td>
                                                        <td>
                                                            <span>168 km</span>
                                                            <div className="progress progress-bar-success mt-1 mb-0">
                                                                <div className="progress-bar" role="progressbar" style={{ width: '70%' }} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </td>
                                                        <td>14:58 26/07/2018</td>
                                                        <td>28/07/2018</td>
                                                    </tr>
                                                    <tr>
                                                        <td>#245689</td>
                                                        <td><i className="fa fa-circle font-small-3 text-danger mr-50"></i>Canceled</td>
                                                        <td className="p-1">
                                                            <ul className="list-unstyled users-list m-0  d-flex align-items-center">
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Vinnie Mostowy" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-5.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="Elicia Rieske" className="avatar pull-up">
                                                                    <img className="media-object rounded-circle" src="./app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" height="30" width="30" />
                                                                </li>
                                                            </ul>
                                                        </td>
                                                        <td>Clifton, Arizona</td>
                                                        <td>
                                                            <span>125 km</span>
                                                            <div className="progress progress-bar-danger mt-1 mb-0">
                                                                <div className="progress-bar" role="progressbar" style={{ width: '95%' }} aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </td>
                                                        <td>14:58 26/07/2018</td>
                                                        <td>28/07/2018</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    );
}

export default Home;