import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import swal from 'sweetalert';
import StringMask from 'string-mask';

import serverApi from '../../services/api';
/* import functions from '../../functions'; */

// import { Container } from './styles';

function Login() {
    const [cnpj, setCnpj] = useState('');
    const [senha, setSenha] = useState('');

    const history = useHistory();

/*     useEffect(() => {
         functions.checkLogin().then(login => {
            if (login) return history.push('/');
        }); 
    }) */

    useEffect(() => {
        //Mask CPNJ
        let nCnpj = cnpj;
        nCnpj = nCnpj.replace(/[^\d]+/g, '');
        nCnpj = (new StringMask('00.000.000/0000-00')).apply(nCnpj);
        if (nCnpj !== cnpj) {
            setCnpj(nCnpj);
        }
    }, [cnpj]);

    async function handleSubmit(e) {
        e.preventDefault();

        const response = await serverApi.login(cnpj, senha);

        if (response.success) {
            localStorage.setItem('authToken', response.token);
            history.push('/');
        } else {
            setSenha('');
            swal('Ops!', response.error, 'error');
        }
    }

    return (
        <div className="app-content content">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
            <div className="content-wrapper">
                <div className="content-header row">
                </div>
                <div className="content-body">
                    <section className="row flexbox-container">
                        <div className="col-xl-12 col-12 d-flex justify-content-center">
                            <div className="card bg-authentication rounded-0 mb-0">
                                <div className="row m-0">
                                    <div className="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                        <img src="./app-assets/images/pages/login.png" alt="branding logo" />
                                    </div>
                                    <div className="col-lg-6 col-12 p-0">
                                        <div className="card rounded-0 mb-0 px-2">
                                            <div className="card-header pb-1">
                                                <div className="card-title">
                                                    <h4 className="mb-0">Entrar</h4>
                                                </div>
                                            </div>
                                            <p className="px-2">Bem-vindo de volta, por favor autentifique-se.</p>
                                            <div className="card-content" style={{ paddingBottom: 14 }}>
                                                <div className="card-body pt-1">
                                                    <form onSubmit={handleSubmit}>
                                                        <fieldset className="form-label-group form-group position-relative has-icon-left">
                                                            <input type="text"
                                                                className="form-control"
                                                                placeholder="CNPJ"
                                                                value={cnpj}
                                                                onChange={e => setCnpj(e.target.value)}
                                                                required />
                                                            <div className="form-control-position">
                                                                <i className="feather icon-user"></i>
                                                            </div>
                                                            <label htmlFor="user-name">CNPJ</label>
                                                        </fieldset>

                                                        <fieldset className="form-label-group position-relative has-icon-left">
                                                            <input type="password"
                                                                className="form-control"
                                                                placeholder="Senha"
                                                                value={senha}
                                                                onChange={e => setSenha(e.target.value)}
                                                                required />
                                                            <div className="form-control-position">
                                                                <i className="feather icon-lock"></i>
                                                            </div>
                                                            <label htmlFor="user-password">Senha</label>
                                                        </fieldset>
                                                        <Link to="/register" className="btn btn-outline-primary float-left btn-inline">Cadastrar</Link>
                                                        <button type="submit" className="btn btn-primary float-right btn-inline">Entrar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div >
        </div >
    );
}

export default Login;