import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import swal from 'sweetalert';
import StringMask from 'string-mask';

import serverApi from '../../services/api';
import viacep from '../../services/viacep';
import functions from '../../functions';

import defaultPic from '../../assets/img/default.png';

function UserEdit() {
    const empresa = useSelector(state => state.empresa);

    const [dados, setDados] = useState({
        razaoSocial: empresa.razaoSocial,
        nomeFantasia: empresa.nomeFantasia,
        inscricaoEstadual: empresa.inscricaoEstadual,
        cep: empresa.cep,
        logradouro: empresa.logradouro,
        numero: empresa.numero,
        complemento: empresa.complemento,
        bairro: empresa.bairro,
        cidade: empresa.cidade,
        uf: empresa.uf,
        email: empresa.email,
        telefone: empresa.telefone
    });

    const [dadosFrete, setDadosFrete] = useState({
        precoPeso: empresa.precoPeso,
        precoCubagem: empresa.precoCubagem,
        precoDistancia: empresa.precoDistancia,
        entregaPessoaFisica: empresa.entregaPessoaFisica
    })

    const [senhaAtual, setSenhaAtual] = useState('');
    const [novaSenha, setNovaSenha] = useState('');
    const [novaSenha2, setNovaSenha2] = useState('');

    const [uploadingImage, setUploadingImage] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        functions.checkLogin().then(login => {
            if (!login) return history.push('/');
            if (JSON.stringify(login) !== JSON.stringify(empresa)) {
                dispatch({ type: 'SET_EMPRESA', empresa: login });
            }
        });
    });

    useEffect(() => {
        //Mask Inscrição Estadual
        let inscricaoEstadual = dados.inscricaoEstadual;
        if (inscricaoEstadual) {
            inscricaoEstadual = inscricaoEstadual.replace(/[^\d\w]+/g, '')
                .toUpperCase();
            if (inscricaoEstadual !== dados.inscricaoEstadual) {
                setDados({ ...dados, inscricaoEstadual });
            }
        }

        //Mask CPNJ
        let cep = dados.cep;
        if (cep) {
            cep = cep.replace(/[^\d]+/g, '');
            cep = (new StringMask('00000-000')).apply(cep);
            if (cep !== dados.cep) {
                setDados({ ...dados, cep });
            }
        }

        //Mask Número
        let numero = dados.numero;
        if (numero) {
            numero = numero.replace(/[^\d]+/g, '');
            if (numero !== dados.numero) {
                setDados({ ...dados, numero });
            }
        }

        //Mask Telefone
        let telefone = dados.telefone;
        if (telefone) {
            telefone = telefone.replace(/[^\d]+/g, '');
            telefone = (new StringMask('(00) 90000-0000')).apply(telefone);
            if (telefone !== dados.telefone) {
                setDados({ ...dados, telefone });
            }
        }
    }, [dados]);

    async function handleEdit1(e) {
        e.preventDefault();

        const response = await serverApi.updateInfo(dados);

        if (response.success) {
            swal('Pronto!', 'Os dados foram editados com sucesso!', 'success');
        } else {
            swal('Ops!', response.error, 'error');
        }

        history.push('/user/edit');
    }

    async function handleEdit2(e) {
        e.preventDefault();

        if (novaSenha !== novaSenha2)
            return swal('Ops!', 'As senhas não coincidem.', 'error');

        const response = await serverApi.changePassword(senhaAtual, novaSenha);

        if (response.success) {
            swal('Pronto!', 'A senha foi alterada com sucesso!', 'success');
        } else {
            swal('Ops!', response.error, 'error');
        }

        setSenhaAtual('');
        setNovaSenha('');
        setNovaSenha2('');
        history.push('/user/edit');
    }

    async function handleEdit3(e) {
        e.preventDefault();

        const response = await serverApi.updateFrete(dadosFrete);

        if (response.success) {
            swal('Pronto!', 'Os dados foram editados com sucesso!', 'success');
        } else {
            swal('Ops!', response.error, 'error');
        }

        history.push('/user/edit');
    }

    async function uploadPhoto(e) {
        if (uploadingImage) return;

        setUploadingImage(true);

        var files = e.target.files;
        console.log('Uploading image');

        if (files.length === 0) return;

        const response = await serverApi.uploadPhoto(files[0]);

        if (response) {
            swal('Pronto!', 'Foto enviada com sucesso!', 'success');
        } else {
            swal('Ops!', 'Não foi possível enviar a foto!', 'error');
        }

        setUploadingImage(false);
    }

    function consultarCEP(noswal = false) {
        if (!dados.cep) {
            if (noswal) return;
            return swal('Ops!', 'CEP inválido!', 'error');
        }

        const cep = dados.cep.replace(/[^\d]+/g, '');

        if (cep.length !== 8) {
            if (noswal) return;
            return swal('Ops!', 'CEP inválido!', 'error');
        }

        viacep.buscarCEP(cep)
            .then(response => {
                if (response.success) {
                    setDados({
                        ...dados,
                        logradouro: response.endereco.logradouro,
                        numero: '',
                        complemento: '',
                        bairro: response.endereco.bairro,
                        cidade: response.endereco.localidade,
                        uf: response.endereco.uf
                    });
                }

                if (noswal) return;
                return swal('Ops!', response.error, 'error');
            });
    }

    return (
        <div className="app-content content">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
            <div className="content-wrapper">
                <div className="content-header row">
                    <div className="content-header-left col-md-9 col-12 mb-2">
                        <div className="row breadcrumbs-top">
                            <div className="col-12">
                                <h2 className="content-header-title float-left mb-0">Minha conta</h2>
                                <div className="breadcrumb-wrapper col-12">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/">Inicio</Link></li>
                                        <li className="breadcrumb-item active">Minha conta</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-body">
                    <section id="page-account-settings">
                        <div className="row">
                            <div className="col-md-3 mb-2 mb-md-0">
                                <ul className="nav nav-pills flex-column mt-md-0 mt-1">
                                    <li className="nav-item">
                                        <a className="nav-link d-flex py-75 active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                                            <i className="feather icon-globe mr-50 font-medium-3"></i>
                                            Geral
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                                            <i className="feather icon-lock mr-50 font-medium-3"></i>
                                            Alterar senha
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link d-flex py-75" id="account-pill-frete" data-toggle="pill" href="#account-vertical-frete" aria-expanded="false">
                                            <i className="feather icon-lock mr-50 font-medium-3"></i>
                                            Preço do Frete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-md-9">
                                <div className="card">
                                    <div className="card-content">
                                        <div className="card-body">
                                            <div className="tab-content">
                                                <div role="tabpanel" className="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                                    <div className="media">
                                                        <a href="# ">
                                                            <img src={Boolean(empresa.foto) ? empresa.foto : defaultPic} className="rounded mr-75" alt="profile" height="64" width="64" />
                                                        </a>
                                                        <div className="media-body mt-75">
                                                            <div className="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                                                <label className="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer waves-effect waves-light" htmlFor="account-upload">Enviar foto</label>
                                                                <input type="file" id="account-upload" accept="image/jpeg, image/png" hidden={true} onChange={e => uploadPhoto(e)} />
                                                                <button className="btn btn-sm btn-outline-warning ml-50 waves-effect waves-light">Resetar</button>
                                                            </div>
                                                            <p className="text-muted ml-75 mt-50">
                                                                <small>Extensões permitidas JPG ou PNG. Tamanho máximo 2MB</small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <form onSubmit={handleEdit1}>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-username">Razão Social</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Razão Social"
                                                                            value={dados.razaoSocial}
                                                                            onChange={e => setDados({ ...dados, razaoSocial: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Nome Fantasia</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Nome Fantasia"
                                                                            value={dados.nomeFantasia}
                                                                            onChange={e => setDados({ ...dados, nomeFantasia: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Inscrição Estadual</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Inscrição Estadual"
                                                                            value={dados.inscricaoEstadual}
                                                                            onChange={e => setDados({ ...dados, inscricaoEstadual: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">CEP</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="CEP"
                                                                            value={dados.cep}
                                                                            onChange={e => setDados({ ...dados, cep: e.target.value })}
                                                                            onBlur={e => consultarCEP(true)}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Logradouro</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Logradouro"
                                                                            value={dados.logradouro}
                                                                            onChange={e => setDados({ ...dados, logradouro: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Número</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Número"
                                                                            value={dados.numero}
                                                                            onChange={e => setDados({ ...dados, numero: e.target.value })}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Complemento</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Complemento"
                                                                            value={dados.complemento}
                                                                            onChange={e => setDados({ ...dados, complemento: e.target.value })}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Bairro</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Bairro"
                                                                            value={dados.bairro}
                                                                            onChange={e => setDados({ ...dados, bairro: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Cidade</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Cidade"
                                                                            value={dados.cidade}
                                                                            onChange={e => setDados({ ...dados, cidade: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">UF</label>
                                                                        <select className="form-control"
                                                                            value={dados.uf}
                                                                            onChange={e => setDados({ ...dados, uf: e.target.value })}
                                                                            required>
                                                                            <option value="">Selecione</option>
                                                                            <option value="AC">AC</option>
                                                                            <option value="AL">AL</option>
                                                                            <option value="AP">AP</option>
                                                                            <option value="AM">AM</option>
                                                                            <option value="BA">BA</option>
                                                                            <option value="CE">CE</option>
                                                                            <option value="DF">DF</option>
                                                                            <option value="ES">ES</option>
                                                                            <option value="GO">GO</option>
                                                                            <option value="MA">MA</option>
                                                                            <option value="MT">MT</option>
                                                                            <option value="MS">MS</option>
                                                                            <option value="MG">MG</option>
                                                                            <option value="PA">PA</option>
                                                                            <option value="PB">PB</option>
                                                                            <option value="PR">PR</option>
                                                                            <option value="PE">PE</option>
                                                                            <option value="PI">PI</option>
                                                                            <option value="RJ">RJ</option>
                                                                            <option value="RN">RN</option>
                                                                            <option value="RS">RS</option>
                                                                            <option value="RO">RO</option>
                                                                            <option value="RR">RR</option>
                                                                            <option value="SC">SC</option>
                                                                            <option value="SP">SP</option>
                                                                            <option value="SE">SE</option>
                                                                            <option value="TO">TO</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">E-mail</label>
                                                                        <input type="email"
                                                                            className="form-control"
                                                                            placeholder="E-mail"
                                                                            value={dados.email}
                                                                            onChange={e => setDados({ ...dados, email: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Telefone</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Telefone"
                                                                            value={dados.telefone}
                                                                            onChange={e => setDados({ ...dados, telefone: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                                <button type="submit" className="btn btn-primary mb-1 mb-sm-0 waves-effect waves-light">Salvar alterações</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div className="tab-pane fade" id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                                    <form onSubmit={handleEdit2}>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-old-password">Senha antiga</label>
                                                                        <input type="password"
                                                                            className="form-control"
                                                                            id="account-old-password"
                                                                            required
                                                                            placeholder="Senha antiga"
                                                                            data-validation-required-message="This old password field is required"
                                                                            value={senhaAtual}
                                                                            onChange={e => setSenhaAtual(e.target.value)} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-new-password">Nova senha</label>
                                                                        <input type="password"
                                                                            name="password"
                                                                            id="account-new-password"
                                                                            className="form-control"
                                                                            placeholder="Nova senha"
                                                                            required
                                                                            data-validation-required-message="The password field is required"
                                                                            value={novaSenha}
                                                                            onChange={e => setNovaSenha(e.target.value)} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-retype-new-password">Confirme a nova senha</label>
                                                                        <input type="password"
                                                                            name="con-password"
                                                                            className="form-control"
                                                                            required
                                                                            id="account-retype-new-password"
                                                                            data-validation-match-match="password"
                                                                            placeholder="Nova senha"
                                                                            data-validation-required-message="The Confirm password field is required"
                                                                            value={novaSenha2}
                                                                            onChange={e => setNovaSenha2(e.target.value)} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                                <button type="submit" className="btn btn-primary mb-1 mb-sm-0 waves-effect waves-light">Salvar alterações</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div className="tab-pane fade" id="account-vertical-frete" role="tabpanel" aria-labelledby="account-pill-frete" aria-expanded="false">
                                                    <form onSubmit={handleEdit3}>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Preço do Peso (R$/kg)</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Preço do Peso (R$/kg)"
                                                                            value={dadosFrete.precoPeso}
                                                                            onChange={e => setDadosFrete({ ...dadosFrete, precoPeso: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Preço da Cubagem (R$/m3)</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Preço da Cubagem (R$/m3)"
                                                                            value={dadosFrete.precoCubagem}
                                                                            onChange={e => setDadosFrete({ ...dadosFrete, precoCubagem: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <div className="controls">
                                                                        <label htmlFor="account-name">Preço da Distância (R$/km)</label>
                                                                        <input type="text"
                                                                            className="form-control"
                                                                            placeholder="Preço do Distância (R$/km)"
                                                                            value={dadosFrete.precoDistancia}
                                                                            onChange={e => setDadosFrete({ ...dadosFrete, precoDistancia: e.target.value })}
                                                                            required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <fieldset className="checkbox">
                                                                        <div className="vs-checkbox-con vs-checkbox-primary">
                                                                            <input type="checkbox"
                                                                                checked={dadosFrete.entregaPessoaFisica}
                                                                                onChange={e => setDadosFrete({ ...dadosFrete, entregaPessoaFisica: e.target.checked })}
                                                                            />
                                                                            <span className="vs-checkbox">
                                                                                <span className="vs-checkbox--check">
                                                                                    <i className="vs-icon feather icon-check"></i>
                                                                                </span>
                                                                            </span>
                                                                            <span className=""> Entrega para pessoa física.</span>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                            <div className="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                                <button type="submit" className="btn btn-primary mb-1 mb-sm-0 waves-effect waves-light">Salvar alterações</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div >
                            </div >
                        </div >
                    </section >
                </div >
            </div >
        </div >
    );
}

export default UserEdit;