import serverApi from './services/api';

async function checkLogin() {
    /* console.log('Verificando autenticação...'); */

    const response = await serverApi.checkLogin();

    if (!response.success) {
        localStorage.removeItem('authToken');
        return false;
    }

    //Converte a foto do banco de dados
    if (response.empresa.foto) {
        const bytes = new Uint8Array(response.empresa.foto.data);
        switch (response.empresa.fotoTipo) {
            case 'jpg':
                response.empresa.foto = 'data:image/jpeg;base64,' + btoa(String.fromCharCode(...bytes));
                break;
            case 'png':
                response.empresa.foto = 'data:image/png;base64,' + btoa(String.fromCharCode(...bytes));
                break;
            default:
                response.empresa.foto = false;
        }
    }

    delete (response.empresa.fotoTipo);

    return response.empresa;
}

export default {
    checkLogin
}