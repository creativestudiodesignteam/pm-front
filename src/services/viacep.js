import axios from 'axios';

export default {
    buscarCEP: async (cep) => {
        try {
            const response = await axios({
                method: 'get',
                url: `https://viacep.com.br/ws/${cep}/json/`
            });

            if (response.erro) {
                return { success: false, error: "CEP inválido." }
            }

            return { success: true, endereco: response.data };
        } catch (err) {
            return { success: false, error: "Não foi possível consultar o CEP." }
        }
    }
}