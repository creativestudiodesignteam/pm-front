import axios from 'axios';

const serverApi = axios.create({
    'baseURL': 'http://api.pmlog.com.br'
});

export default {
    register: async (dados) => {
        try {
            await serverApi.post('/cadastro', dados);

            return { success: true }
        } catch (error) {
            return { success: false, error: error.response.data };
        }
    },

    login: async (cnpj, senha) => {
        try {
            const response = await serverApi.post('/login', { cnpj, senha });
            return { success: true, token: response.data.token }
        } catch (error) {
            return { success: false, error: error.response.data };
        }
    },

    checkLogin: async () => {
        try {
            const response = await serverApi.get('/empresa', {
                headers: {
                    authorization: localStorage.getItem('authToken')
                }
            });

            return { success: true, empresa: response.data }
        } catch (error) {
            if (error.response) {
                return { success: false, error: error.response.data };
            }

            return { success: false, error: 'Não foi possível realizar o login.' };
        }
    },

    updateInfo: async (dados) => {
        try {
            await serverApi.post('/empresa/editar', dados, {
                headers: {
                    authorization: localStorage.getItem('authToken')
                }
            });

            return { success: true }
        } catch (error) {
            return { success: false, error: error.response.data };
        }
    },

    changePassword: async (senhaAtual, novaSenha) => {
        try {
            await serverApi.post('/empresa/alterar_senha', {
                senhaAtual,
                novaSenha
            }, {
                headers: {
                    authorization: localStorage.getItem('authToken')
                }
            });

            return { success: true }
        } catch (error) {
            return { success: false, error: error.response.data };
        }
    },

    updateFrete: async (dados) => {
        try {
            await serverApi.post('/empresa/editar/frete', dados, {
                headers: {
                    authorization: localStorage.getItem('authToken')
                }
            });

            return { success: true }
        } catch (error) {
            return { success: false, error: error.response.data };
        }
    },

    uploadPhoto: async (foto) => {
        const data = new FormData();
        data.append('foto', foto);

        try {
            await serverApi.post('/empresa/alterar_foto', data, {
                headers: {
                    authorization: localStorage.getItem('authToken')
                }
            });

            return true
        } catch (error) {
            return false;
        }
    }
}