const actions = [];

actions['SET_EMPRESA'] = (state, action) => {
    return { ...state, empresa: action.empresa };
}

actions['SET_LOADING'] = (state, action) => {
    return { ...state, loading: action.loading };
}

export default actions;