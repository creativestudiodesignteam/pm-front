import { createStore } from 'redux';
import actions from './actions';

const INITIAL_STATE = {
    loading: false,
    empresa: false
};

function reducer(state, action) {
    if (action.type) {
        const a = actions[action.type];
        if (a) {
            return a(state, action);
        }
    }

    if (!state) {
        return INITIAL_STATE;
    }

    return state;
}

const store = createStore(reducer);

export default store;