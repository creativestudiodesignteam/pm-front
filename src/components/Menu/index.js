import React from 'react';
import { Link } from 'react-router-dom';

function Menu() {
    return (
        <>
            <div className="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
                <div className="navbar-header">
                    <ul className="nav navbar-nav flex-row">
                        <li className="nav-item mr-auto">
                            <Link className="navbar-brand" to="/">
                                <div className="brand-logo"></div>
                                <h2 className="brand-text mb-0">Vuexy</h2>
                            </Link>
                        </li>
                        <li className="nav-item nav-toggle">
                            <a className="nav-link modern-nav-toggle pr-0" data-toggle="collapse" href="# ">
                                <i className="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="shadow-bottom"></div>
                <div className="main-menu-content">
                    <ul className="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        <li className=" nav-item">
                            <Link to="/">
                                <i className="feather icon-home"></i>
                                <span className="menu-title" data-i18n="Email">Início</span>
                            </Link>
                        </li>
                        <li className=" navigation-header">
                            <span>Apps</span>
                        </li>
                        <li className="nav-item">
                            <a href="# ">
                                <i className="feather icon-mail"></i>
                                <span className="menu-title" data-i18n="Email">Email</span>
                            </a>
                        </li>
                        <li className=" nav-item">
                            <a href="# ">
                                <i className="feather icon-message-square"></i>
                                <span className="menu-title" data-i18n="Chat">Chat</span>
                            </a>
                        </li>
                        <li className=" nav-item">
                            <a href="# ">
                                <i className="feather icon-check-square"></i>
                                <span className="menu-title" data-i18n="Todo">Todo</span>
                            </a>
                        </li>
                        <li className=" nav-item">
                            <a href="# ">
                                <i className="feather icon-calendar"></i>
                                <span className="menu-title" data-i18n="Calender">Calender</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a href="# ">
                                <i className="feather icon-shopping-cart"></i>
                                <span className="menu-title" data-i18n="Ecommerce">Ecommerce</span>
                            </a>
                            <ul className="menu-content">
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="Shop">Shop</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="Details">Details</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="Wish List">Wish List</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="Checkout">Checkout</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a href="# ">
                                <i className="feather icon-user"></i>
                                <span className="menu-title" data-i18n="User">User</span>
                            </a>
                            <ul className="menu-content">
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="List">List</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="View">View</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="# ">
                                        <i className="feather icon-circle"></i>
                                        <span className="menu-item" data-i18n="Edit">Edit</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
}

export default Menu;