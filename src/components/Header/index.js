import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import defaultPic from '../../assets/img/default.png';

function Header() {
    
    const empresa = useSelector(state => state.empresa);
    const history = useHistory();
    function Logout() {
        localStorage.removeItem('authToken');
        history.push('/login');
    }
    return (
        <nav className="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
            <div className="navbar-wrapper">
                <div className="navbar-container content">
                    <div className="navbar-collapse" id="navbar-mobile">
                        <div className="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                            <ul className="nav navbar-nav">
                                <li className="nav-item mobile-menu d-xl-none mr-auto">
                                    <a className="nav-link nav-menu-main menu-toggle hidden-xs" href="# ">
                                        <i className="ficon feather icon-menu"></i>
                                    </a>
                                </li>
                            </ul>
                            {/* <ul className="nav navbar-nav bookmark-icons">
                                    <li className="nav-item d-none d-lg-block">
                                        <a className="nav-link" href="# " data-toggle="tooltip" data-placement="top" title="Todo">
                                            <i className="ficon feather icon-check-square"></i>
                                        </a>
                                    </li>
                                    <li className="nav-item d-none d-lg-block">
                                        <a className="nav-link" href="# " data-toggle="tooltip" data-placement="top" title="Chat">
                                            <i className="ficon feather icon-message-square"></i>
                                        </a>
                                    </li>
                                    <li className="nav-item d-none d-lg-block">
                                        <a className="nav-link" href="# " data-toggle="tooltip" data-placement="top" title="Email">
                                            <i className="ficon feather icon-mail"></i>
                                        </a>
                                    </li>
                                    <li className="nav-item d-none d-lg-block">
                                        <a className="nav-link" href="# " data-toggle="tooltip" data-placement="top" title="Calendar">
                                            <i className="ficon feather icon-calendar"></i>
                                        </a>
                                    </li>
                                </ul> */}
                        </div>
                        <ul className="nav navbar-nav float-right">
                            <li className="dropdown dropdown-user nav-item">
                                <a className="dropdown-toggle nav-link dropdown-user-link" href="# " data-toggle="dropdown">
                                    <div className="user-nav d-sm-flex d-none">
                                        <span className="user-name text-bold-600">{empresa.nomeFantasia}</span>
                                        <span className="user-status">
                                            {empresa.cnpj ?
                                                empresa.cnpj.slice(0, 2) + '.' +
                                                empresa.cnpj.slice(2, 5) + '.' +
                                                empresa.cnpj.slice(5, 8) + '/' +
                                                empresa.cnpj.slice(8, 12) + '-' +
                                                empresa.cnpj.slice(12, 14)
                                                : '00.000.000/0000-00'}
                                        </span>
                                    </div>
                                    <span>
                                        <img className="round" src={empresa.foto ? empresa.foto : defaultPic} alt="avatar" height="40" width="40" />
                                    </span>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <Link className="dropdown-item" to="/user/edit">
                                        <i className="feather icon-user"></i> Editar Perfil
                                        </Link>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="# " onClick={e => Logout()}>
                                        <i className="feather icon-power"></i> Sair
                                        </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default Header;